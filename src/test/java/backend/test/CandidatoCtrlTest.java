package backend.test;

import backend.test.services.CandidatoService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.jupiter.api.Assertions.*;

class CandidatoCtrlTest {

    @Autowired
    CandidatoService candidatoService;


    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void addCandidato() {
    }

    @Test
    void getAllCandidatos() {
    }

    @Test
    void getCandidatosById() {
    }

    @Test
    void deleteCandidato() {
    }

    @Test
    void updateCandidato() {
    }
}