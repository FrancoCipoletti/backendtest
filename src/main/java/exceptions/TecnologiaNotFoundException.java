package exceptions;

public class TecnologiaNotFoundException  extends RuntimeException{
    public TecnologiaNotFoundException(String message) {
        super(message);
    }
}
