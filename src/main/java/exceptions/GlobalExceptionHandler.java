package exceptions;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@ControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

    @ExceptionHandler(RuntimeException.class)
    @ResponseBody
    public String runtimeException(RuntimeException e) {
        log.info ("Manejador de excepciones globales");
        log.error("error:", e);
        return "RuntimeException:" + e.getMessage();
    }

    @ExceptionHandler(Throwable.class)
    @ResponseBody
    public String throwable(Throwable e) {
        log.info("Manejador de excepciones globales");
        log.error("error:", e);
        return "Throwable: " + e.getMessage();

    }}
