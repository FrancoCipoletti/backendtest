package exceptions;

public class CandidatoNotFoundException extends RuntimeException{
    public CandidatoNotFoundException(String message) {
        super(message);
    }
}
