package backend.test;


import backend.test.dto.Mensaje;
import backend.test.model.Candidato;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.web.bind.annotation.*;
import backend.test.services.CandidatoService;

import java.util.List;
import java.util.Objects;

@RestController

@RequestMapping("/candidato")

public class CandidatoCtrl {

    private final CandidatoService candidatoService;

    public CandidatoCtrl(CandidatoService candidatoService) {
        this.candidatoService = candidatoService;
    }

    @PostMapping("/add")
    public ResponseEntity<Candidato> addCandidato(@RequestBody Candidato candidato){
        String mensaje;
        if (StringUtils.isBlank((CharSequence) candidato.getNombre()) || StringUtils.isBlank((CharSequence) candidato.getApellido())) {
            mensaje = ( "Nombre y Apellido no pueden ser vacios");}
        else if(!Objects.equals(candidato.getTipoDni(), "DNI") && !Objects.equals(candidato.getTipoDni(), "LE") && !Objects.equals(candidato.getTipoDni(), "LC")){
            mensaje = ( "Tipo Documento solo puede ser DNI, LE , LC");
        }
        else if (Objects.isNull(candidato.getDni() ) || candidato.getDni()<0){
            mensaje = ( "DNI no puede ser nulo o negativo");}
        else if  (StringUtils.isBlank( candidato.getFechaNacimiento().toString())){
            mensaje = ( "Fecha Nacimiento no puede ser vacia");}
        else {
        Candidato newCandidato = candidatoService.addCandidato(candidato);
            TestApplication.logger.info("Se creo el candidato:"+ candidato.toString());
        return new ResponseEntity<>(newCandidato,HttpStatus.CREATED);}
        TestApplication.logger.info("No se creo el candidato debido a :"+ mensaje);
        return new ResponseEntity(new Mensaje(mensaje), HttpStatus.BAD_REQUEST);
    }


    @GetMapping("/todos")
    public ResponseEntity<List<Candidato>> getAllCandidatos() {
        List<Candidato> candidatos = candidatoService.findAllCandidatos();
        return new ResponseEntity<>(candidatos, HttpStatus.OK);
    }

    @GetMapping("/{id_Candidato}")
    public ResponseEntity<Candidato> getCandidatosById(@PathVariable("id_Candidato") Long id_Candidato) {
        Candidato candidato = candidatoService.findCandidatoById(id_Candidato);
        return new ResponseEntity<>(candidato, HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Candidato> deleteCandidato(@PathVariable("id") Long id){
        candidatoService.deleteCandidato(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping("/update")
    public ResponseEntity<Candidato> updateCandidato(@RequestBody Candidato candidato){
        Candidato updateCandidato = candidatoService.updateCandidato(candidato);
        return new ResponseEntity<>(updateCandidato,HttpStatus.OK);
    }

}
