package backend.test;

import backend.test.dto.Mensaje;
import backend.test.model.Experiencia;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import backend.test.services.ExperienciaService;
import java.util.List;
import java.util.Objects;

@RestController

@RequestMapping("/experiencia")
public class ExperienciaCtrl {

    private final ExperienciaService experienciaService;
    public ExperienciaCtrl(ExperienciaService experienciaService) {
        this.experienciaService = experienciaService;
    }

    @PostMapping("/add")
    public ResponseEntity<Experiencia> addExperiencia(@RequestBody Experiencia experiencia){
        String mensaje;
        if (StringUtils.isBlank( experiencia.getFechaInicio().toString())) {
            mensaje = ( "Fecha inicio no puede estar vacia");}
        else if (Objects.isNull(experiencia.getTecnologia().getId())){
            mensaje = ( "Id tecnologia no puede ser nulo");}
        else if  (Objects.isNull(experiencia.getCandidato().getId())){
            mensaje = ( "Id Candidato no puede ser nulo");}
        else {
            Experiencia newExperiencia = experienciaService.addExperiencia(experiencia);
            TestApplication.logger.info("Se creo la siguiente experiencia:"+ experiencia.toString());
            return new ResponseEntity<>(newExperiencia, HttpStatus.CREATED);
        }
        TestApplication.logger.info("No se pudo crear la exp debido a:" + mensaje);
        return new ResponseEntity(new Mensaje(mensaje), HttpStatus.BAD_REQUEST);

    }


    @GetMapping("/tec/{nombre}")
    public ResponseEntity<List<Experiencia>> getExperienciaByNombre(@PathVariable("nombre") String nombre) {
        List<Experiencia> experiencia = experienciaService.findByNombre(nombre);
        return new ResponseEntity<>(experiencia, HttpStatus.OK);
    }
}
