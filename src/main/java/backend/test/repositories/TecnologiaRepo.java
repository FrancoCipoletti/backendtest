package backend.test.repositories;

import backend.test.model.Tecnologia;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

public interface TecnologiaRepo extends JpaRepository<Tecnologia,Long> {
    @Transactional
    void deleteTecnologiaById(Long id);
    Optional<Tecnologia> findTecnologiaById(Long id);
}
