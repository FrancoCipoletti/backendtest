package backend.test.repositories;

import backend.test.model.Candidato;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

public interface CandidatoRepo extends JpaRepository<Candidato,Long> {

    Optional<Candidato> findCandidatoById(Long id);
    @Transactional
    void deleteCandidatoById(Long id);
}
