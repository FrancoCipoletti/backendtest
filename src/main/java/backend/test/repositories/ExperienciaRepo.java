package backend.test.repositories;

import backend.test.model.Experiencia;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import java.util.List;

public interface ExperienciaRepo extends JpaRepository<Experiencia,Long> {

    @Query(value = "SELECT * FROM Experiencia inner join Tecnologia on Tecnologia.id_Tecnologia=Experiencia.id_Tecnologia where CONCAT(Tecnologia.nombre,Tecnologia.version) like CONCAT(:nombre,'%')  ", nativeQuery = true)
    List<Experiencia> findByNombre(@Param(value="nombre") String nombre);

}
