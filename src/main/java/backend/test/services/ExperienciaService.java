package backend.test.services;


import backend.test.model.Experiencia;
import org.springframework.beans.factory.annotation.Autowired;
import backend.test.repositories.ExperienciaRepo;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ExperienciaService {
    private final ExperienciaRepo experienciaRepo;

    @Autowired
    public ExperienciaService(ExperienciaRepo experienciaRepo) {
        this.experienciaRepo = experienciaRepo;
    }

    public List<Experiencia> findAllExperiencias(){
        return experienciaRepo.findAll();
    }

    public Experiencia addExperiencia (Experiencia experiencia) {
        return experienciaRepo.save(experiencia);
    }

    public List<Experiencia> findByNombre(String nombre){ return  experienciaRepo.findByNombre(nombre);};
}
