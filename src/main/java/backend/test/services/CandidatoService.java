package backend.test.services;

import backend.test.model.Candidato;
import backend.test.repositories.CandidatoRepo;
import exceptions.CandidatoNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class CandidatoService {

    private final CandidatoRepo candidatoRepo;

    @Autowired
    public CandidatoService(CandidatoRepo candidatoRepo) {
        this.candidatoRepo = candidatoRepo;
    }

    public List<Candidato> findAllCandidatos(){
        return candidatoRepo.findAll();
    }

    public Candidato findCandidatoById(Long id_Candidato){
        return candidatoRepo.findCandidatoById(id_Candidato).orElseThrow(()-> new CandidatoNotFoundException("Articulo con id "+ id_Candidato + "no existe."));
    }

    public Candidato addCandidato (Candidato candidato) {
        return candidatoRepo.save(candidato);
    }

    public void deleteCandidato(Long id){
        candidatoRepo.deleteCandidatoById(id);

    }
    public Candidato updateCandidato(Candidato candidato){
        return candidatoRepo.save(candidato);
    }
}
