package backend.test.services;

import backend.test.model.Tecnologia;
import exceptions.TecnologiaNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import backend.test.repositories.TecnologiaRepo;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TecnologiaService {
    private final TecnologiaRepo tecnologiaRepo;

    @Autowired
    public TecnologiaService(TecnologiaRepo tecnologiaRepo) {
        this.tecnologiaRepo = tecnologiaRepo;
    }

    public List<Tecnologia> findAllTecnologias(){
        return tecnologiaRepo.findAll();
    }


    public Tecnologia findTecnologiaById(Long id){
        return tecnologiaRepo.findTecnologiaById(id).orElseThrow(()-> new TecnologiaNotFoundException("Articulo con id "+ id + "no existe."));
    }
    public void deleteTecnologia(Long id){
        tecnologiaRepo.deleteTecnologiaById(id);

    }
    public Tecnologia addTecnologia (Tecnologia tecnologia) {
        return tecnologiaRepo.save(tecnologia);
    }

    public Tecnologia updateTecnologia(Tecnologia tecnologia){
        return tecnologiaRepo.save(tecnologia);
    }
}
