package backend.test;

import backend.test.dto.Mensaje;
import backend.test.model.Tecnologia;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import backend.test.services.TecnologiaService;

import java.util.List;
import java.util.Objects;

@RestController

@RequestMapping("/tecnologia")
public class TecnologiaCtrl {

    private final TecnologiaService tecnologiaService;


    public TecnologiaCtrl(TecnologiaService tecnologiaService) {
        this.tecnologiaService = tecnologiaService;
    }

    @PostMapping("/add")
    public ResponseEntity<Tecnologia> addTecnologia(@RequestBody Tecnologia tecnologia){
        String mensaje;
        if (StringUtils.isBlank((CharSequence) tecnologia.getNombre())  ) {
            mensaje = ( "Nombre  no puede  ser vacio ");}
        else if (Objects.isNull(tecnologia.getVersion() ) || tecnologia.getVersion()<0){
            mensaje = ( "Mensaje  no puede  ser vacio o menor a 0");}
        else {
        Tecnologia newTecnologia = tecnologiaService.addTecnologia(tecnologia);
        TestApplication.logger.info("Se creo la siguiente tecnologia:"+ tecnologia.toString());
        return new ResponseEntity<>(newTecnologia, HttpStatus.CREATED);}
        TestApplication.logger.info("No se creo la tecnologia debido a :"+ mensaje);
        return new ResponseEntity(new Mensaje(mensaje), HttpStatus.BAD_REQUEST);
    }


    @GetMapping("/todos")
    public ResponseEntity<List<Tecnologia>> getAllTecnologias() {
        List<Tecnologia> tecnologias = tecnologiaService.findAllTecnologias();
        return new ResponseEntity<>(tecnologias, HttpStatus.OK);
    }

    @GetMapping("/{id_Tecnologia}")
    public ResponseEntity<Tecnologia> findTecnologiaById(@PathVariable("id_Tecnologia") Long id_Tecnologia) {
        Tecnologia tecnologia = tecnologiaService.findTecnologiaById(id_Tecnologia);
        return new ResponseEntity<>(tecnologia, HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Tecnologia> deleteTecnologia(@PathVariable("id") Long id){
        tecnologiaService.deleteTecnologia(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping("/update")
    public ResponseEntity<Tecnologia> updateTecnologia(@RequestBody Tecnologia tecnologia){
        Tecnologia updateTecnologia = tecnologiaService.updateTecnologia(tecnologia);
        return new ResponseEntity<>(updateTecnologia,HttpStatus.OK);
    }
}
