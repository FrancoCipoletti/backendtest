package backend.test.model;

import javax.persistence.*;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

@Entity
@Table
public class Experiencia implements Serializable {

    @Id
    @Column(name="id_experiencia")
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id_experiencia;

    @Column(name="fechaInicio",nullable = false)
    private Date fechaInicio;

    @Override
    public String toString() {
        return "Experiencia{" +
                "id_experiencia=" + id_experiencia +
                ", fechaInicio=" + fechaInicio +
                ", fechaFin=" + fechaFin +
                ", candidato=" + candidato +
                ", tecnologia=" + tecnologia +
                '}';
    }

    @Column(name="fechaFin",nullable = true)
    private Date fechaFin;

    @ManyToOne
    @JoinColumn(name = "id_candidato")
    private Candidato candidato;

    public Candidato getCandidato() {
        return candidato;
    }


    public void setCandidato(Candidato candidato) {
        this.candidato = candidato;
    }

    public void setTecnologia(Tecnologia tecnologia) {
        this.tecnologia = tecnologia;
    }

    public Tecnologia getTecnologia() {
        return tecnologia;
    }



    @ManyToOne
    @JoinColumn(name = "id_Tecnologia")
    private Tecnologia tecnologia;

    public Long getId() {
        return id_experiencia;
    }


    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public double getAosDeExperiencia() {
        Date tempFechaFin = null;
        if (this.fechaFin == null) {
            SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd 'at' HH:mm:ss z");
            tempFechaFin = new Date(System.currentTimeMillis());
        } else {
            tempFechaFin = this.fechaFin;
        }
        return Double.parseDouble(String.valueOf((tempFechaFin.getTime() - this.fechaInicio.getTime() )/ (1000 * 60 * 60 * 24))) / 365 ;
    }
}
