package backend.test.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class TipoDni implements Serializable {

    @Id
    @Column(name="id")
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    @Column(name="nombre",nullable = false)
    private String nombre;
}
