package backend.test.model;

import javax.persistence.*;
import java.io.Serializable;
@Entity
@Table
public class Tecnologia implements Serializable {

    @Id
    @Column(name="id_Tecnologia")
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id ;

    @Column(name="nombre",nullable = false)
    private String nombre;

    @Override
    public String toString() {
        return "Tecnologia{" +
                "id=" + id +
                ", nombre='" + nombre + '\'' +
                ", version=" + version +
                '}';
    }

    @Column(name="version",nullable = false)
    private Integer version;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }
}
